To install enter the folder using the terminal and type the following:

sudo python setup.py install

enter your password and hit return key.

The installer will ask you where you want to have your data files installed.
Default is your home directory.

The installer has been tested on Mac OSX 10.8 and up, Debian Wheezy/Sid, Linux Mint 16, Ubuntu 13.04 and Fedora 20.
May work for other distros as well but you might find yourself having to install many dependencies manually.
PySam installation has been causing problems for some users because some dependencies were missing (zlib compression libraries mostly).

If you discover a bug, leave a note in the issue tracker. Please include as much information about the problem as you possibly can, including all error messages.

Note that some of the pyCRAC tools have undergone a number of changes since version 1.2. All of these changes have been highlighted in the updated manual. 
The changes do NOT affect compatibility with previous versions.

Sander Granneman

Granneman Lab
University of Edinburgh, UK
Department of Synthetic and Systems Biology (SynthSys)
