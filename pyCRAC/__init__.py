import sys

if sys.version[0:3] < '2.7' : raise ImportError('pyCRAC requires Python Version 2.7 or above')
if sys.version[0:3] >= '3.0': raise ImportError('pyCRAC is not compatible with Python 3.0 or higher')
