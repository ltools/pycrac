import sys

if sys.version[0:3] < '2.7':
	raise ImportError('Python Version 2.7 or above is required for pyCRAC')
if sys.version[0:3] >= '3.0':
	raise ImportError('pyCRAC is not compatible with Python 3.0 or higher')
